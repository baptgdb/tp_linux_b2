# 3. fail2ban

- ce module est la suite du module [2. Réplication de la base de données](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/2.%20R%C3%A9plication%20de%20la%20base%20de%20donn%C3%A9es)

## Sommaire

[TOC]


### I. installation de epel-release et fail2ban.

sudo dnf install -y epel-release
sudo dnf install fail2ban fail2ban-firewalld

sudo systemctl start fail2ban
sudo systemctl enable fail2ban
```
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
```

sudo systemctl status fail2ban
```
Active: active (running)
```

### II. configuration de fail2ban.

- on modifient les fichiers de configuration.

- sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

- on modivient les valeurs du fichier /etc/fail2ban/jail.local 

- sudo nano /etc/fail2ban/jail.local
```
...
[DEFAULT]
...
bantime = 1h
...
findtime = 1h
...
maxretry = 3
...
```

- sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local

- sudo systemctl restart fail2ban

### III. Securing SSH service with Fail2ban

- sudo nano /etc/fail2ban/jail.d/sshd.local
- on ajoute :
```
[sshd]
enabled = true

# Override the default global configuration
# for specific jail sshd
bantime = 1d
maxretry = 3
```

- sudo systemctl restart fail2ban

- sudo fail2ban-client status
```
Status
|- Number of jail:      1
`- Jail list:   sshd
```

- sudo fail2ban-client get sshd maxretry
```
3
```

### IV. testes 

- on teste entre le proxy et web:
ssh baptiste@10.101.1.11
```
baptiste@10.101.1.11's password:
Permission denied, please try again.
baptiste@10.101.1.11's password:
Permission denied, please try again.
baptiste@10.101.1.11's password:
baptiste@10.101.1.11: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
```
- On retest :
ssh baptiste@10.101.1.11
```
ssh: connect to host 10.101.1.11 port 22: Connection refused
```

sudo fail2ban-client status sshd
```
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     3
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 1
   |- Total banned:     1
   `- Banned IP list:   10.101.1.13
```

- web banne l'ip du proxy

- pour débannir l'ip du proxy:
sudo fail2ban-client unban 10.101.1.13
```
1
```

- l'ip du proxy à été débannie :

sudo fail2ban-client status sshd
```
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     3
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     1
   `- Banned IP list:
```

- Ensuite, On refait les étapes précédentes sur les autres machines.

## Fin du module **3. fail2ban**.
- voici le module suivant: [4. netdata](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/4.%20netdata)
