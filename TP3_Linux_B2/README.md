# TP 3 linux B2

- Dans ce TP on reprend le [TP 2](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/tp2_linux_b2) et on l'améliore :

- Ce tp comporte 4 module :

    - 1. le **revers proxy** permet à un utilisateur d'Internet d'accéder à des serveurs internes, (Nextcloud dans notre exemple).
        - [1. revers proxy](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/1.%20revers%20proxy)


    - 2. le **Réplication de la base de données** permet de répliquer la base de donnée **entre la base de donnée** et la sauvegarde.
        - [2. Réplication de la base de données](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/2.%20R%C3%A9plication%20de%20la%20base%20de%20donn%C3%A9es)


    - 3. **Fail2ban** permet de prévenir contre les tentatives d'intrusions.
        - [3. fail2ban](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/3.%20fail2ban)


    - 4. **Netdata** est un outil conçu pour collecter l'utilisation du processeur, l'activité du disque, l'utilisation de la bande passante, etc.
        - [4. netdata](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/4.%20netdata)

    - ## Fin.
