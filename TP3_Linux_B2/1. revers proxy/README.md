# 1. revers proxy

- ce module est le premier de ce [TP](https://gitlab.com/baptgdb/tp_linux_b2/-/blob/main/TP3_Linux_B2/README.md)

## Sommaire

[TOC]

### I. installation d'nginx

- l'adresse de **proxy.tp3.linux** est **10.102.1.13**.

- sudo dnf install -y nginx

- sudo systemctl enable nginx
```
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
```
- sudo systemctl start nginx
- sudo systemctl status nginx
```
active (running)
```

- sudo ss -altnp
```
State    Recv-Q   Send-Q       Local Address:Port       Peer Address:Port   Process
LISTEN   0        511                0.0.0.0:80              0.0.0.0:*       users:(("nginx",pid=1388,fd=6),("nginx",pid=1387,fd=6))
LISTEN   0        511                   [::]:80                 [::]:*       users:(("nginx",pid=1388,fd=7),("nginx",pid=1387,fd=7))
```

sudo ps -ef | grep nginx
```
root        1387       1  0 16:13 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1388    1387  0 16:13 ?        00:00:00 nginx: worker process
baptiste    1401    1181  0 16:15 pts/0    00:00:00 grep --color=auto nginx
```
### II. configuration d'nginx.
nano /etc/nginx/conf.d/revers_proxy
```
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://10.101.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}

```
- on enlève le bloque "server" de /etc/nginx/nginx.conf
- sudo systemctl restart nginx
- sudo firewall-cmd --add-port=80/tcp --permanent
- sudo firewall-cmd --reload
- dans web:
    - web.tp2.linux je remplace l'ip par web.tp2.linux dans /var/www/tp2_nextcloud/config/config.php
- si on essaie avec l'ip :
![](https://i.imgur.com/QuXcgJ2.png)

- web peut ping le revers proxy:
```
ping 10.101.1.13
PING 10.101.1.13 (10.101.1.13) 56(84) bytes of data.
64 bytes from 10.101.1.13: icmp_seq=1 ttl=64 time=0.322 ms
64 bytes from 10.101.1.13: icmp_seq=2 ttl=64 time=1.28 ms
64 bytes from 10.101.1.13: icmp_seq=3 ttl=64 time=0.395 ms
--- 10.101.1.13 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2047ms
rtt min/avg/max/mdev = 0.322/0.664/1.275/0.433 ms
```

- revers proxy peut ping le web:
```
ping 10.101.1.11
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.393 ms
64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=1.06 ms
64 bytes from 10.101.1.11: icmp_seq=3 ttl=64 time=1.19 ms
--- 10.101.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2042ms
rtt min/avg/max/mdev = 0.393/0.880/1.191/0.348 ms
```
### III. HTTPS
```
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
```
Avec la commande précédent: inscrire le nom du nom d'hote:
```
Common Name (eg, your name or your server's hostname) []:web.tp2.linux
```
sudo mv server.crt web.tp2.linux.crt
sudo mv server.key web.tp2.linux.key
sudo mv web.tp2.linux.* /srv/
sudo ls -al /etc/pki/tls/
```
total 16
drwxr-xr-x. 5 root root   104 Nov 15 00:27 .
drwxr-xr-x. 7 root root    75 Nov 15 00:19 ..
lrwxrwxrwx. 1 root root    49 Sep 20 18:35 cert.pem -> /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
drwxr-xr-x. 2 root root    54 Nov 15 00:27 certs
-rw-r--r--. 1 root root   412 Nov  1 19:35 ct_log_list.cnf
drwxr-xr-x. 2 root root     6 Nov  1 19:39 misc
-rw-r--r--. 1 root root 12163 Nov  1 19:35 openssl.cnf
drwxr-xr-x. 2 root root     6 Nov  1 19:39 private
```
cd /etc/nginx/

- on met la clé et le certificat dans /etc/pki/tls/
```
sudo mv web.tp2.linux.crt /etc/pki/tls/
sudo mv web.tp2.linux.key /etc/pki/tls/
```
sudo nano /etc/nginx/conf.d/revers_proxy.conf
```
listen 443 ssl;
    ssl_certificate     /etc/pki/tls/web.tp2.linux.crt
    ssl_certificate_key /etc/pki/tls/web.tp2.linux.key
...
# à la fin après le '}'
server {

    listen 80 default_server;


    server_name web.tp2.linux;


    return 301 https://$host$request_uri;

}
```
### IV. Firewall

sudo firewall-cmd --add-port=443/tcp --permanent
success

sudo firewall-cmd --reload
success

### V. Tests
- ça fonctionne :

![](https://i.imgur.com/i2fsUYY.png)

## fin du module **revers proxy**
- suite : [2. Réplication de la base de données ](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/2.%20R%C3%A9plication%20de%20la%20base%20de%20donn%C3%A9es)
