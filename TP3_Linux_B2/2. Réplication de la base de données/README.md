# 2. Réplication de la base de données

- ce module est la suite du module [1. revers proxy](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/1.%20revers%20proxy).

## Sommaire

[TOC]

## I. configuration de la base de données.
### 1. installation de mariadb.

- **vm backup** = 10.102.1.14

- sudo dnf install mariadb-server -y

- sudo systemctl start mariadb

- sudo systemctl enable mariadb

- sudo dnf install unzip -y

- sudo dnf install zip -y

- sudo mysql_secure_installation
```
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...        //azerty

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

### 2. mysql

- sudo nano /etc/my.cnf.d/mariadb-server.cnf
```
[mysqld]
# add follows in [mysqld] section : get binary logs

log-bin=mysql-bin
# define server ID (uniq one)

server-id=101
```

- sudo systemctl restart mariadb                                                               

- sudo mysql -u root -p
```
grant replication slave on *.* to repl_user@'%' identified by 'password'; 
flush privileges;
exit
```

- sudo nano /etc/my.cnf.d/mariadb-server.cnf
```
# add follows in [mysqld] section : get binary logs

log-bin=mysql-bin
# define server ID (uniq one)

server-id=102
# read only yes

read_only=1
# define own hostname

report-host=backup.tp3.linux
```
- systemctl restart mariadb 

## II. configuration de la database.

- mkdir systemctl restart mariadb 
- sudo mariabackup --backup --target-dir /home/mariadb_backup -u root -p password
```
[00] 2022-11-18 16:35:12 Connecting to MySQL server host: localhost, user: root, password: set, port: not set, socket: not set
[00] 2022-11-18 16:35:12 Using server version 10.5.16-MariaDB-log
mariabackup based on MariaDB server 10.5.16-MariaDB Linux (x86_64)
[00] 2022-11-18 16:35:12 uses posix_fadvise().
[00] 2022-11-18 16:35:12 cd to /var/lib/mysql/
[00] 2022-11-18 16:35:12 open files limit requested 0, set to 1024
[00] 2022-11-18 16:35:12 mariabackup: using the following InnoDB configuration:
[00] 2022-11-18 16:35:12 innodb_data_home_dir =
[00] 2022-11-18 16:35:12 innodb_data_file_path = ibdata1:12M:autoextend
[00] 2022-11-18 16:35:12 innodb_log_group_home_dir = ./
[00] 2022-11-18 16:35:12 InnoDB: Using Linux native AIO
2022-11-18 16:35:12 0 [Note] InnoDB: Number of pools: 1
mariabackup: Can't create file '/home/mariadb_backup/ib_logfile0' (errno: 17 "File exists")
[00] 2022-11-18 16:35:12 Error: failed to open the target stream for 'ib_logfile0'.
```

- sudo zip -r mariadb_backup.zip mariadb_backup/

## III. retour a la configuration de la base de données.

- mise en place de "Secure Shell File Transfer Protocol" pour récupérer le fichier mariadb_backup.zip sur la base de donnée.

- sftp baptiste@10.101.1.12
```
sftp> get /home/mariadb_backup.zip
sftp> quit
```

- sudo unzip mariadb_backup.zip

- ls -al mariadb_backup.zip
```
-rw-r--r--. 1 baptiste baptiste 852903 Nov 18 16:42 mariadb_backup.zip
```

- sudo mariabackup --prepare --target-dir ~/mariadb_backup`
```
mariabackup based on MariaDB server 10.5.16-MariaDB Linux (x86_64)
[00] 2022-11-18 16:50:18 cd to /home/baptiste/mariadb_backup/
[00] 2022-11-18 16:50:18 open files limit requested 0, set to 1024
[00] 2022-11-18 16:50:18 This target seems to be not prepared yet.
[00] 2022-11-18 16:50:18 mariabackup: using the following InnoDB configuration for recovery:
[00] 2022-11-18 16:50:18 innodb_data_home_dir = .
[00] 2022-11-18 16:50:18 innodb_data_file_path = ibdata1:12M:autoextend
[00] 2022-11-18 16:50:18 innodb_log_group_home_dir = .
[00] 2022-11-18 16:50:18 InnoDB: Using Linux native AIO
[00] 2022-11-18 16:50:18 Starting InnoDB instance for recovery.
[00] 2022-11-18 16:50:18 mariabackup: Using 104857600 bytes for buffer pool (set by --use-memory parameter)
2022-11-18 16:50:18 0 [Note] InnoDB: Uses event mutexes
2022-11-18 16:50:18 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
2022-11-18 16:50:18 0 [Note] InnoDB: Number of pools: 1
2022-11-18 16:50:18 0 [Note] InnoDB: Using crc32 + pclmulqdq instructions
2022-11-18 16:50:18 0 [Note] InnoDB: Using Linux native AIO
2022-11-18 16:50:18 0 [Note] InnoDB: Initializing buffer pool, total size = 104857600, chunk size = 104857600
2022-11-18 16:50:18 0 [Note] InnoDB: Completed initialization of buffer pool
2022-11-18 16:50:18 0 [Note] InnoDB: Starting crash recovery from checkpoint LSN=1759020,1759020
[00] 2022-11-18 16:50:18 Last binlog file , position 0
[00] 2022-11-18 16:50:18 completed OK!
```

- sudo mariabackup --copy-back --target-dir ~/mariadb_backup
```
...
[01] 2022-11-18 16:52:05 Copying ib_buffer_pool to /var/lib/mysql/ib_buffer_pool
[01] 2022-11-18 16:52:05         ...done
[00] 2022-11-18 16:52:05 completed OK!
```

- sudo chown -R mysql. /var/lib/mysql
- sudo systemctl start mariadb 

- les journaux binaires:
- sudo cat ~/mariadb_backup/xtrabackup_binlog_info
```
mysql-bin.000001        665     []
mysql-bin.000002        389     []
mysql-bin.000003        385     0-101-2
```

- mysql -u root -p 
```
MariaDB [(none)]> change master to 
master_host='10.101.1.12',
master_user='repl_user',
master_password='password',
master_log_file='mysql-bin.000001',
master_log_pos=642;

MariaDB [(none)]> start slave; 

MariaDB [(none)]> show slave status\G 

*************************** 1. row ***************************
                Slave_IO_State: Waiting for master to send event
                   Master_Host: 10.101.1.12
                   Master_User: repl_user
                   Master_Port: 3306
                 Connect_Retry: 60
               Master_Log_File: mysql-bin.000007
           Read_Master_Log_Pos: 342
                Relay_Log_File: mariadb-relay-bin.000009
                 Relay_Log_Pos: 641
         Relay_Master_Log_File: mysql-bin.000007
              Slave_IO_Running: Yes
             Slave_SQL_Running: Yes
               Replicate_Do_DB:
           Replicate_Ignore_DB:
            Replicate_Do_Table:
        Replicate_Ignore_Table:
       Replicate_Wild_Do_Table:
   Replicate_Wild_Ignore_Table:
                    Last_Errno: 0
                    Last_Error:
                  Skip_Counter: 0
           Exec_Master_Log_Pos: 342
               Relay_Log_Space: 1337
               Until_Condition: None
                Until_Log_File:
                 Until_Log_Pos: 0
            Master_SSL_Allowed: No
            Master_SSL_CA_File:
            Master_SSL_CA_Path:
               Master_SSL_Cert:
             Master_SSL_Cipher:
                Master_SSL_Key:
         Seconds_Behind_Master: 0
 Master_SSL_Verify_Server_Cert: No
                 Last_IO_Errno: 0
                 Last_IO_Error:
                Last_SQL_Errno: 0
                Last_SQL_Error:
   Replicate_Ignore_Server_Ids:
              Master_Server_Id: 101
                Master_SSL_Crl:
            Master_SSL_Crlpath:
                    Using_Gtid: No
                   Gtid_IO_Pos:
       Replicate_Do_Domain_Ids:
   Replicate_Ignore_Domain_Ids:
                 Parallel_Mode: optimistic
                     SQL_Delay: 0
           SQL_Remaining_Delay: NULL
       Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
              Slave_DDL_Groups: 0
Slave_Non_Transactional_Groups: 0
    Slave_Transactional_Groups: 0
1 row in set (0.000 sec)
```
- **show slave status\G** la connexion entre le serveur réplique et le serveur source

## IV. test de la base de donnée.
### 1. depuis la base de donnée.
```
CREATE DATABASE test_database
create database yolooooooo;
```

### 2. depuis la backup.

- show databases;

```
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
| test_database      |
| yolooooooo         |
+--------------------+
6 rows in set (0.000 sec)
```

## Fin du module **2. Réplication de la base de données**
- voici le module suivant: [3. fail2ban](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/3.%20fail2ban)
