# 4. netdata

- ce module est la suite du module [3. fail2ban](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP3_Linux_B2/3.%20fail2ban)

## Sommaire

[TOC]

## I. Netdata.

- sur web.tp2.linux, db.tp2.linux et backup.tp2.linux

- on commence sur web.tp2.linux 

## II. installation de Netdata.

- sudo dnf install -y wget
- wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --disable-telemetry

- sudo systemctl start netdata
- sudo systemctl enable netdata
- sudo systemctl status netdata
```
Active: active (running)
```

## III. Les Firewall.

sudo firewall-cmd --permanent --add-port=19999/tcp
sudo firewall-cmd --reload

## IV. les testes

![](https://i.imgur.com/nwQvO2o.png)

## V. On ajoute ça a discord

- On ajoute les [webhook](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks) a discord.

- cd /etc/netdata
- sudo nano ./edit-config health_alarm_notify.conf
- on ajout "https://discordapp.com/api/webhooks/1044289213459349544/3eoKvoIY-P6DhlcOAxkJSOAaIyjWV-LKwjpwt1oR5RoYw4ko2Q02Ja-t4emlX9Mf40Nj"
```
...
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/104428921345934954>
...
DEFAULT_RECIPIENT_DISCORD="nextcloud_1"
```

sudo systemctl restart netdata

## VI. test de stress, voyons si ça fonctionne :

- sudo dnf install stress

- sudo dnf install stress-ng

- stress --cpu 8 --io 4 --vm 2 --vm-bytes 128M --timeout 10m

![](https://i.imgur.com/JvVf8pL.png)

## Fin du module **4. netdata**.
- ce module est le dernier de ce [TP](https://gitlab.com/baptgdb/tp_linux_b2/-/blob/main/TP3_Linux_B2/README.md)
