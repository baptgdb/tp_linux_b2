# **tp n°1 linux b2**

[TOC]

## I. configuration des VM

- 10.101.1.11
- sudo hostname node1.tp1.b2
sudo nano /etc/hostname
```
node1.tp1.b2
```

- 10.101.1.12
- sudo hostname node2.tp1.b2
sudo nano /etc/hostname
```
node2.tp1.b2
```

### 1.1 Mise a jour et installation.

- sudo dnf update -y

- sudo dnf install -y python3 vim 

### 1.2. Désactivation de SELinux.

sudo setenforce 0
sudo nano /etc/selinux/config
```
SELINUX=permissive
```

### 1.3. configuration réseau.

sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
```
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0
DNS=1.1.1.1
```
- sudo nmcli con reload

- sudo nmcli con up "System enp0s8"

- sudo systemctl restart NetworkManager

### 1.4 configuration des firewall.
- **sur node1.tp1.b2**

sudo firewall-cmd --list-all
```
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### 1.5 Dans la liste des firewalls nous avons des services que nous n'utilisons pas.
 
sudo firewall-cmd --remove-service cockpit --permanent
```
success
```

sudo firewall-cmd --remove-service dhcpv6-client --permanent
```
success
```

sudo firewall-cmd --remove-service ssh --permanent
```
success
```

sudo firewall-cmd --reload
```
success
```

sudo firewall-cmd --list-all
```
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  ```
- les services "cockpit", "dhcpv6-client" et "ssh" ne sont plus.


### 1.6 SSH.
#### 1. SSH, port 22 du firewall.

sudo firewall-cmd --add-port=22/tcp --permanent
```
success
```

sudo firewall-cmd --reload
```
success
```

#### 2. clé.
* génération d'une paire de clé ssh depuis "git Bash"
ssh-keygen -t rsa -b 4096

* copie de la clé ssh vers node1.tp1.b2
```
ssh-copy-id baptiste@10.101.1.11
```
* copie de la clé ssh vers node2.tp1.b2
```
ssh-copy-id baptiste@10.101.1.12
```
- le nom d'hôte de 10.101.1.11:
```
sudo hostnamectl set-hostname node1.tp1.b2
sudo reboot
```

- le nom d'hôte de 10.101.1.11:
```
sudo hostnamectl set-hostname node2.tp1.b2
sudo reboot
```

### 3. DNS.

sudo nano /etc/resolv.conf
```
nameserver 1.1.1.1
```

### 4. nom d'hôte.

sudo nano /etc/hosts
```
10.101.1.11 node1
```

sudo nano /etc/hosts
```
10.101.1.12 node2
```

### 4.1. Test ping + nom d'hôte.

ping node1
```
PING node1 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1 (10.101.1.11): icmp_seq=1 ttl=64 time=0.921 ms
...
64 bytes from node1 (10.101.1.11): icmp_seq=6 ttl=64 time=1.24 ms
--- node1 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5095ms
rtt min/avg/max/mdev = 0.324/0.877/1.263/0.341 ms
```
on peur ping "node2" depuis "node1".

## II. création d'un utilisateur.

### 1. création et configuration d'un utilisateur.

- useradd toto
- passwd toto :
```
Changing password for user toto.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
```
- On ajoute le groupe "admins" :
```
sudo groupadd admins
```

### 2. création d'un groupe "admins" pour l'utilisateur "toto". 

- On peut donner les mêmes permissions au groupe "admins" qu'au groupe "wheel" via l'utilitaire de vérification visudo avec la commande "sudo visudo" :
```
## Same thing without a password
# %wheel        ALL=(ALL)       NOPASSWD: ALL
# %admins        ALL=(ALL)       NOPASSWD: ALL
```

- On ajoute l'utilisateur "toto" au groupe "admins":
```
sudo usermod -aG admins toto 
```
- On peut voir le répertoire de l'utilisateur toto dans le répertoire "/home" avec la commande "ls":
- voici l'id de l'utiisateur de "toto":
```
id toto
uid=1001(toto) gid=1001(toto) groups=1001(toto),10(wheel),1003(admins)
```

### 3. test des permissions de "toto".

sudo dnf upgrade -y
```
Last metadata expiration check: 2:07:57 ago on Mon 14 Nov 2022 02:39:11 PM CET.
Dependencies resolved.
Nothing to do.
Complete!
```

## III. Partitionnement
- **sur node1.tp1.b2**

- On ajoute deux disques durs de 3 Go chacun :
    - sdb
    - sdc



- Nous pouvons lister caractéristiques des disques et de leurs partitions, avec la commande "lsblk" :
```
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0   20G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0   19G  0 part
  ├─rl-root 253:0    0   17G  0 lvm  /
  └─rl-swap 253:1    0    2G  0 lvm  [SWAP]
sdb           8:16   0    6G  0 disk	<---
sdc           8:32   0    6G  0 disk	<---
sr0          11:0    1 1024M  0 rom
```
- On ajoute le fichier "part1", "part2" et "part3" depuis le repertoir /mnt/

### 1. Nous créons deux "volumes Physiques".

sudo pvcreate /dev/sdb
```
Physical volume "/dev/sdb" successfully created.
```
sudo pvcreate /dev/sdc
```
Physical volume "/dev/sdb" successfully created.
```

sudo pvs
```
  PV         VG Fmt  Attr PSize   PFree
  /dev/sda2  rl lvm2 a--  <19.00g    0
  /dev/sdb      lvm2 ---    6.00g 6.00g	   <---
  /dev/sdc      lvm2 ---    6.00g 6.00g    <---
```

### 2. Nous créons un "Volume Group".

sudo vgcreate data /dev/sdb
```
Volume group "data" successfully created
```
sudo vgcreate data /dev/sdc
```
A volume group called data already exists.
```

sudo pvs
```
  data   1   0   0 wz--n-  <6.00g <6.00g    <---
  rl     1   2   0 wz--n- <19.00g     0
```

### 3. Nous créons un "volumes logiques".

sudo lvcreate -L 1G data -n data1 
```
Logical volume "data1" created.
```
sudo lvcreate -L 1G data -n data2 
```
Logical volume "data2" created.
```
sudo lvcreate -L 1G data -n data3
```
Logical volume "data3" created.
```

sudo lvs 
```
  LV    VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  data1 data -wi-a-----   1.00g

  data2 data -wi-a-----   1.00g

  data3 data -wi-a-----   1.00g

  root  rl   -wi-ao---- <17.00g

  swap  rl   -wi-ao----   2.00g
```

### 4. Formater et monter des partitions.

#### 1. Formater une partition exemple de data1

sudo mkfs -t ext4 /dev/data/data1
```
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: e19ea6b4-de82-4d1f-9dc3-dcfe664e1366
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

#### 2. data2 & data3

pour la suite il suffit de refaire la meme chose pour /dev/data/data2 et /dev/data/data3.

#### 3. Monter la partition data1.

- *On monte les partitions part1 jusqu'à part3 à data1 jusqu'à data3*:
```
sudo mount /dev/data/data1 /mnt/part1
sudo mount /dev/data/data2 /mnt/part2
sudo mount /dev/data/data3 /mnt/part3
```

df -h
```
Filesystem              Size  Used Avail Use% Mounted on
devtmpfs                889M     0  889M   0% /dev
tmpfs                   907M     0  907M   0% /dev/shm
tmpfs                   907M  8.5M  898M   1% /run
tmpfs                   907M     0  907M   0% /sys/fs/cgroup
/dev/mapper/rl-root      17G  2.5G   15G  15% /
/dev/sda1              1014M  254M  761M  26% /boot
tmpfs                   182M     0  182M   0% /run/user/1000
/dev/mapper/data-data1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/data-data2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/data-data3  976M  2.6M  907M   1% /mnt/part3
```
- Comme on peut le voir, les partitions ont bien été monté.

#### 4. montage automatique au boot de la machine.

sudo nano /etc/fstab
```
...
[...]
/dev/data/data1 /mnt/part1 ext4 defaults 0 0
/dev/data/data2 /mnt/part2 ext4 defaults 0 0
/dev/data/data3 /mnt/part3 ext4 defaults 0 0

```

#### 4.1. test du montage automatique au boot de la machine.

- sudo umount /mnt/part1
- sudo mount -av

```
/mnt/part1               : successfully mounted
/mnt/part2               : already mounted
/mnt/part3               : already mounted
```
- sudo reboot

## IV.  Création d'un service web.
### 1. Créer un fichier qui définit une unité de service:
on ajoute le fichier web.service à /etc/systemd/system
sudo nano web.service
```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

```
### 2. tcp, port 8888 du firewall.

- systemd relie les fichiers de configuration.
```
sudo firewall-cmd --add-port=8888/tcp --permanent
success
```

### 2.1 on peut interagir avec notre unité :

- sudo systemctl daemon-reload
- sudo systemctl start web
- sudo systemctl enable web

- On peut tester avec **"curl 10.101.1.11:8888"** :
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
on refait les mêmes étapes que toto pour web:
- sudo useradd web 
- sudo passwd web 
- mkdir var/www/meow
```
ls -al

drwxr-xr-x.  2 root root    6 Nov 14 19:11 meow
```
### 2.2. modifications des permissions pour le fichier meow/

- sudo chgrp web meow/
- sudo chmod 700 meow/
voici le résultat :
ls -al
```
drwx------.  2 root web     6 Nov 14 19:11 meow
```

sudo nano web.service
```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/var/www/meow/

[Install]
WantedBy=multi-user.target
```

### 3. test Final.
- On peut tester avec **"curl 10.101.1.11:8888"** pour vérifier si ça fonctionne avec les dernières modifications:
- curl 10.101.1.11:8888
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

# **FIN**
