# web.tp2.linux

## Sommaire

[TOC]

## I. HTTPD

instalation du service httpd
```
sudo dnf install -y httpd
```

On vire les commentaires du fichier httpd.conf
```
sudo vim /etc/httpd/conf/httpd.conf
```

on tape la commande :
```
g/^ *#.*/d
```

### 1 systemctl HTTPD

sudo systemctl start httpd.service

sudo systemctl enable httpd.service

sudo systemctl status httpd.service


sudo ss -alnpt
```
State   Recv-Q  Send-Q   Local Address:Port     Peer Address:Port  		Process
LISTEN   0        511            *:80                  *:*       		users:(("httpd",pid=4459,fd=4),("httpd",pid=4458,fd=4),("httpd",pid=4457,fd=4),("httpd",pid=4455,fd=4))
```

curl localhost
```
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
	...
<a href="https://rockylinux.org/" id="rocky-poweredby"><img src="icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

sudo firewall-cmd --add-port=80/tcp --permanent

sudo firewall-cmd --reload

# utilisateur d'apache

cat /etc/httpd/conf/httpd.conf
```
User apache
Group apache
```

cat /etc/passwd | grep apache
```
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
```

le fichier /usr/share/testpage/index.html est accessible en lecture
```
ls -al
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```

# créez un nouvel utilisateur

useradd web

sudo usermod -aG apache web

# Changer l'utilisateur utilisé par Apache

cat /etc/httpd/conf/httpd.conf
```
...
ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User web
Group apache
...
```

ps -ef | grep httpd
```
root        1594       1  0 16:06 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         1595    1594  0 16:06 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         1596    1594  0 16:06 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         1597    1594  0 16:06 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         1598    1594  0 16:06 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
baptiste    1830    1459  0 16:09 pts/0    00:00:00 grep --color=auto httpd
```
- le nouvelle utilisateur est "web"

# Changer le port d'Apache

- on modifient le fichier [/etc/httpd/conf/httpd.conf](https://gitlab.com/baptgdb/tp_linux_b2/-/blob/main/TP2_Linux_B2/httpd.conf.txt)
cat /etc/httpd/conf/httpd.conf
```
Listen 88
```

sudo firewall-cmd --add-port=88/tcp --permanent
```
success
```

sudo firewall-cmd --reload
```
success
```

sudo systemctl restart httpd.service 


sudo ss -alnpt
```
State    Recv-Q   Send-Q       Local Address:Port       Peer Address:Port   Process
LISTEN   0        128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=698,fd=3))
LISTEN   0        128                   [::]:22                 [::]:*       users:(("sshd",pid=698,fd=4))
LISTEN   0        511                      *:88                    *:*       users:(("httpd",pid=1898,fd=4),("httpd",pid=1897,fd=4),("httpd",pid=1896,fd=4),("httpd",pid=1894,fd=4))
```


curl 10.102.1.11:88
```
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
	...
      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```



## Install de MariaDB 

### **db.tp2.linux**

### 1. Installation 
sudo dnf install mariadb-server
sudo systemctl enable mariadb
sudo systemctl start mariadb
sudo mysql_secure_installation
```
Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.
```

sudo ss -altunp
```
Netid       State        Recv-Q       Send-Q               Local Address:Port               Peer Address:Port       Process
tcp         LISTEN       0            128                        0.0.0.0:22                      0.0.0.0:*           users:(("sshd",pid=718,fd=3))
tcp         LISTEN       0            128                           [::]:22                         [::]:*           users:(("sshd",pid=718,fd=4))
tcp         LISTEN       0            80                               *:3306                          *:*           users:(("mariadbd",pid=3661,fd=19))
```

## 2. mysql

sudo mysql -u root -p
```
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
FLUSH PRIVILEGES;
```

## 3. firewall

- sudo firewall-cmd --add-port=3306/tcp --permanent
- sudo firewall-cmd --reload


### **web.tp2.linux**

## 1. test
mysql -u nextcloud -h 10.102.1.12 -p
```
 Host 'db.tp2.linux' is not allowed to connect to this MariaDB server
```


sudo dnf provides mysql
```
mysql-8.0.30-3.el9_0.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.30-3.el9_0
```

dnf install mariadb-server
```
SHOW DATABASES;
use CHARACTER;
Access denied for user 'nextcloud'@'10.102.1.11' to database 'CHARACTER'
```

### db.tp2.linux

MariaDB [(none)]> SELECT User, Host FROM mysql.user;
```
+-------------+-------------+
| User        | Host        |
+-------------+-------------+
| nextcloud   | 10.102.1.11 |
| nextcloud   | 10.102.1.11 |
| mariadb.sys | localhost   |
| mysql       | localhost   |
| root        | localhost   |
+-------------+-------------+
5 rows in set (0.001 sec)
```

## PHP
### web.tp2.linux

- installation :
```
sudo dnf config-manager --set-enabled crb
sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
dnf module list php
sudo dnf module enable php:remi-8.1 -y
sudo dnf install -y php81-php
sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
sudo dnf install -y unzip
```
- sudo mkdir /var/www/tp2_nextcloud/

- dans /var/www/tp2_nextcloud/ 
```
sudo curl -SLO https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
```

- sudo unzip nextcloud-25.0.0rc3.zip -x

- sudo rm nextcloud-25.0.0rc3.zip
- ls -a
```
.  ..  nextcloud
```

### configuration 

- copie des fichiers de nextcloud vers la position actuelle.
- sudo mv nextcloud/* .
- copie des fichiers caché de nextcloud vers la position actuelle.
- sudo mv nextcloud/.* .

- ls -a
```
.         config       dist        nextcloud     public.php  themes
..        console.php  .htaccess   occ           remote.php  updater
3rdparty  COPYING      index.html  ocm-provider  resources   .user.ini
apps      core         index.php   ocs           robots.txt  version.php
AUTHORS   cron.php     lib         ocs-provider  status.php
```

correction de l'erreur ("sudo mkdir /var/www/tp2_nextcloud/" au lieu de "mkdir /var/www/tp2_nextcloud/")

sudo chown -R apache:apache /var/www/tp2_nextcloud

dans /etc/httpd/conf.d
mkdir nextcloudweb.conf

sudo systemctl restart httpd.service


- sur windows:

nano c:\Windows\System32\Drivers\etc\hosts
```
10.102.1.11 web.tp2.linux

```
### Test.

- ça fonctionne avec l'ip et le dommaine :

l'utilisateur est paramétré pour l'utilisateur nommé arbitrairement root avec comme password azerty.

## voila :

![](https://i.imgur.com/ZRTOdZW.png)

# fin 
