# TP 5

Groupe :

[GADEBILLE Baptiste](https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP5_Linux_B2)

[EVEILLARD Thomas](https://gitlab.com/Fireginger/b2-linux-tp/-/blob/main/TP5/README.md)

## sommaire

[TOC]

## 0. Prérequis

- 3 vm à jour sous **rocky 8**
    - selinux en permissive

| nom des VM | IP des VM |
| -------- | -------- |
| mattermost | 10.105.1.2 |
| data | 10.105.1.3 |
| proxy | 10.105.1.4 |

## 0.1. Objectif du projet 

- Le but de ce projet est d'installer Mattermost, 
- Mattermost est un service de messagerie instantanée libre auto-hébergeable qui est présenté comme une alternative à discord et Microsoft Teams. 
- Dans ce projet nous avons un reversé proxy en amont du serveur qui contient le service mattermost et une base de données séparée, on utilise Netdata pour le monitoring et fail2 bans pour se prémunir contre les intrusions.

## I. La base de donnée

| nom des VM | IP des VM |
| -------- | -------- |
| data | 10.105.1.3 |

### 1. Install MySQL 8.0 on Rocky Linux 8

- on instale et lance mysql:
    - ```sudo dnf install -y mysql```

    - ```sudo yum module disable mysql```

    - ```sudo dnf module enable mysql:8.0```

    - ```sudo dnf install @mysql -y```

- mysql --version
```
mysql  Ver 8.0.30 for Linux on x86_64 (Source distribution)
```

### 2. Manage MySQL 8.0 Service

- ```sudo systemctl start mysqld```

- ```sudo systemctl enable mysqld```

- ```sudo systemctl status mysqld```
```
...
Active: active (running)
...
```

### 3. Secure MySQL Installation

- ```mysql_secure_installation```
```
Securing the MySQL server deployment.

Connecting to MySQL using a blank password.

VALIDATE PASSWORD COMPONENT can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD component?

Press y|Y for Yes, any other key for No: no
Please set the password for root here.

New password: azerty

Re-enter new password: azerty
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : yes
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : yes
Success.

By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) : yes
 - Dropping test database...
Success.

 - Removing privileges on test database...
Success.

Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : yes
Success.

All done!
```

### 4. Connect to the MySQL Server

- ```mysql -u root -p```

- ```mysql> SELECT VERSION ();```
```

+------------+
| VERSION () |
+------------+
| 8.0.30     |
+------------+
1 row in set (0.00 sec)

```
- on crée la base de donnée "mattermost"

    - mysql> ```CREATE DATABASE mattermost;```
    
- on crée l'utilisateur "mattermost"

    - mysql> ```CREATE USER 'mattermost'@'10.105.1.2' IDENTIFIED BY 'UnCodePasSuper';```
    
- on accorde tous les privilèges à un l'utilisateur mattermost, ce qui donne à cet utilisateur un contrôle total sur une base de données mattermost.*

    - mysql> ```GRANT ALL ON mattermost.* TO 'mattermost'@'10.105.1.2';```
    
- on recharge la tables de privilège

    - mysql> ```FLUSH PRIVILEGES;```

- on liste les utilisateurs

    - mysql> ```select user from mysql.user;```
```

+------------------+
| user             |
+------------------+
| mattermost       |
| mysql.infoschema |
| mysql.session    |
| mysql.sys        |
| root             |
+------------------+
5 rows in set (0.00 sec)

```
- on liste les bases de données

    - mysql> ```SHOW DATABASES;```
```

+--------------------+
| Database           |
+--------------------+
| information_schema |
| mattermost         |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.00 sec)

```
- on sort de mysql

    - mysql> ```EXIT;```

### 4. Firewall

- On ouvre le port tcp 3306

    - ```sudo firewall-cmd --add-port=3306/tcp --permanent``` 

    - ```sudo firewall-cmd --reload```

## II. mattermost

| nom des VM | IP des VM |
| -------- | -------- |
| mattermost | 10.105.1.2 |


### 1. Install Mattermost server

- on installe le server Mattermost 

    - ```sudo dnf install -y wget```

    - ```wget https://releases.mattermost.com/7.5.1/mattermost-7.5.1-linux-amd64.tar.gz```

    - ```tar -xvzf *.gz```

- ```ls -a```
```
.   .bash_history  .bash_profile  mattermost                           .ssh
..  .bash_logout   .bashrc        mattermost-7.5.1-linux-amd64.tar.gz
```
- on met mattermost dans le répertoire /opt et on ajoute le dossier data

    - ```sudo rm -f mattermost-7.5.1-linux-amd64.tar.gz```

    - ```sudo mv mattermost /opt```

    - ```sudo mkdir /opt/mattermost/data```

### 2. mattermost user 

- les droits de l'utilisateur mattermost : 

    - on ajouter l'utilisateur à son groupe

	    - ```sudo useradd --system --user-group mattermost```

	- on modifie les permissions du dossier /opt/mattermost

	    - ```sudo chown -R mattermost:mattermost /opt/mattermost```

	    - ```sudo chmod -R g+w /opt/mattermost```

	    - ```sudo restorecon -R /opt/mattermost```

### 3. Firewall

- On ouvre le port tcp 8065

    - ```sudo firewall-cmd --add-port=8065/tcp --permanent```

    - ```sudo firewall-cmd --reload```
    
### 4. config for mattermost json file

- voici le contenu du fichier avant qu'on le modifie [config.json](https://gitlab.com/baptgdb/tp_linux_b2/-/blob/main/TP5_Linux_B2/config.json)

    - ```cat /opt/mattermost/config/config.json```
```
...
    "DriverName": "postgres",
    "DataSource": "postgres://mmuser:mostest@localhost/mattermost?charset=utf8mb4,utf8&writeTimeout=30s"
...
```

- voici le contenu modifié du fichier [config.json](https://gitlab.com/baptgdb/tp_linux_b2/-/blob/main/TP5_Linux_B2/config.json)

    - ```sudo nano /opt/mattermost/config/config.json```
```
...
    "DriverName": "mysql",
    "DataSource": "mattermost:UnCodePasSuper@tcp(10.105.1.3:3306)/mattermost?charset=utf8mb4,utf8&writeTimeout=30s",
...

```
- nous allons dans le dossier mattermost
- 
    - ```cd /opt/mattermost```
    
- on lance le serveur mattermost

    - ```sudo -u mattermost ./bin/mattermost```

**la page se lancer bien :** 

![](https://i.imgur.com/OTlksQg.png)


### 5. Set up Mattermost as a service

- on créent le service Mattermost

    - ```sudo touch /etc/systemd/system/mattermost.service```

    - ```sudo nano /etc/systemd/system/mattermost.service```
```
[Unit]
Description=Mattermost
After=syslog.target network.target postgresql.service

[Service]
Type=notify
WorkingDirectory=/opt/mattermost
User=mattermost
ExecStart=/opt/mattermost/bin/mattermost
PIDFile=/var/spool/mattermost/pid/master.pid
TimeoutStartSec=3600
KillMode=mixed
LimitNOFILE=49152

[Install]
WantedBy=multi-user.target
```

- ```sudo chmod 644 /etc/systemd/system/mattermost.service```

- ```sudo systemctl daemon-reload```

- ```sudo systemctl enable mattermost```

- ```sudo systemctl start mattermost```

- ```sudo systemctl status mattermost```
```
...
Active: active (running)
...
```

- ```sudo systemctl restart mattermost```

- ```curl http://localhost:8065```
```
curl http://localhost:8065
<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0"><meta name="robots" content="noindex, nofollow"><meta name="referrer" content="no-referrer"><title>Mattermost</title><meta name="mobile-web-app-capable" content="yes"><meta name="application-name" content="Mattermost"><meta name="format-detection" content="telephone=no"><link rel="icon" type="image/png" href="/static/images/favicon/favicon-default-16x16.png" sizes="16x16"><link rel="icon" type="image/png" href="/static/images/favicon/favicon-default-24x24.png" sizes="24x24"><link rel="icon" type="image/png" href="/static/images/favicon/favicon-default-32x32.png" sizes="32x32"><link rel="icon" type="image/png" href="/static/images/favicon/favicon-default-64x64.png" sizes="64x64"><link rel="icon" type="image/png" href="/static/images/favicon/favicon-default-96x96.png" sizes="96x96"><link rel="stylesheet" class="code_theme"><style>.error-screen{font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;padding-top:50px;max-width:750px;font-size:14px;color:#333;margin:auto;display:none;line-height:1.5}.error-screen h2{font-size:30px;font-weight:400;line-height:1.2}.error-screen ul{padding-left:15px;line-height:1.7;margin-top:0;margin-bottom:10px}.error-screen hr{color:#ddd;margin-top:20px;margin-bottom:20px;border:0;border-top:1px solid #eee}.error-screen-visible{display:block}</style><meta http-equiv="Content-Security-Policy" content="script-src 'self' cdn.rudderlabs.com/ js.stripe.com/v3"><script defer="defer" src="/static/main.94bef8661903342eb099.js"></script><meta name="apple-mobile-web-app-title" content="Mattermost" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="default" /><link rel="apple-touch-icon" sizes="76x76" href="/static/icon_76x76.png" /><link rel="apple-touch-icon" sizes="72x72" href="/static/icon_72x72.png" /><link rel="apple-touch-icon" sizes="60x60" href="/static/icon_60x60.png" /><link rel="apple-touch-icon" sizes="57x57" href="/static/icon_57x57.png" /><link rel="apple-touch-icon" sizes="152x152" href="/static/icon_152x152.png" /><link rel="apple-touch-icon" sizes="144x144" href="/static/icon_144x144.png" /><link rel="apple-touch-icon" sizes="120x120" href="/static/icon_120x120.png" /><link rel="manifest" href="/static/manifest.json" /></head><body class="font--open_sans enable-animations"><div id="root"><div class="error-screen"><h2>Cannot connect to Mattermost</h2><hr/><p>We're having trouble connecting to Mattermost. If refreshing this page (Ctrl+R or Command+R) does not work, please verify that your computer is connected to the internet.</p><br/></div><div class="loading-screen" style="position:relative"><div class="loading__content"><div class="round round-1"></div><div class="round round-2"></div><div class="round round-3"></div></div></div></div><div id="root-portal"></div><noscript>To use Mattermost, please enable JavaScript.</noscript></body></html>
```

## III. proxy

| nom des VM | IP des VM |
| -------- | -------- |
| proxy | 10.105.1.4 |

### 1. installation d'nginx

- ```sudo dnf install nginx -y```

- ```sudo systemctl enable nginx```

- ```sudo systemctl start nginx```

- ```sudo systemctl status nginx```
```
...
Active: active (running)
...
```

### 2. Firewall

- On ouvre le port tcp 80

    - ```sudo firewall-cmd --add-port=80/tcp --permanent```

    - ```sudo firewall-cmd --reload```
    
### 3. configuration d'nginx

- on crée un fichier de conf additionnels nginx dans conf.d
    - ```sudo cat /etc/nginx/conf.d/proxy.conf```
```
server {
# On indique le nom que client va saisir pour accéder au service
    server_name mattermost_tp_linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.105.1.2:8065;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

### 4. fichier host

- on modifie le fichier host de Windows :
    - C:\Windows\System32\drivers\etc\hosts
```
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
10.105.1.4 mattermost_tp_linux
```

### IV. HTTPS

| nom des VM | IP des VM |
| -------- | -------- |
| proxy | 10.105.1.4 |

### 1. clés et certificat

-  on génère une paire clés certificat sur le serveur proxy
    - ```openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt```
```
Country Name (2 letter code) [XX]:fr
State or Province Name (full name) []:aquitaine
Locality Name (eg, city) [Default City]:bordeaux
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:mattermost_tp_linux
Email Address []:
```
- on met le certificat dans "/etc/pki/tls/certs"

    - ```sudo mv server.crt /etc/pki/tls/certs/;```

- on met la clés dans "/etc/pki/tls/private/"

    - ```sudo mv server.key /etc/pki/tls/private/;```

- ```cat /etc/nginx/conf.d/proxy.conf```
```
listen 443 ssl;
    ssl_certificate     /etc/pki/tls/web.tp2.linux.crt
    ssl_certificate_key /etc/pki/tls/web.tp2.linux.key

server {
# On indique le nom que client va saisir pour accéder au service
    server_name mattermost_tp_linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.105.1.2:8065;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

### 2. firewall tcp au port 443

- ```sudo firewall-cmd --add-port=443/tcp --permanent```

- ```sudo firewall-cmd --reload```

on arrive sur notre site en tapant "https://mattermost_tp_linux/"

## V. fail2ban

| nom des VM | IP des VM |
| -------- | -------- |
| mattermost | 10.105.1.2 |
| data | 10.105.1.3 |
| proxy | 10.105.1.4 |

### 1. installation de epel-release et fail2ban sur mattermost et data.


- ```sudo dnf install -y epel-release```

- ```sudo dnf install fail2ban fail2ban-firewalld```

- ```sudo systemctl start fail2ban```

- ```sudo systemctl enable fail2ban```
```
...
Active: active (running)
...
```

### 2. configuration de fail2ban.

- ```sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local```

- ```sudo nano /etc/fail2ban/jail.local```
```
...
[DEFAULT]
...
bantime = 1h
...
findtime = 1h
...
maxretry = 3
...
```

- ```sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local```

- ```sudo systemctl restart fail2ban```

### 3. Securing SSH service with Fail2ban

- ```sudo nano /etc/fail2ban/jail.d/sshd.local```
```
[sshd]
enabled = true

# Override the default global configuration
# for specific jail sshd
bantime = 1d
maxretry = 3
```

- ```sudo fail2ban-client status```
```
Status
|- Number of jail:      1
`- Jail list:   sshd
```

- ```sudo fail2ban-client get sshd maxretry```
```
3
```

## VI. netdata

| nom des VM | IP des VM |
| -------- | -------- |
| mattermost | 10.105.1.2 |
| data | 10.105.1.3 |
| proxy | 10.105.1.4 |

### 1. installation de Netdata sur mattermost et data.

- ```sudo dnf install -y wget```

- ```wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --disable-telemetry```

- ```sudo systemctl start netdata```

- ```sudo systemctl enable netdata```

- ```sudo systemctl status netdata```
```
...
Active: active (running)
...
```

### 2. Les Firewall.

- ```sudo firewall-cmd --permanent --add-port=19999/tcp```

- ```sudo firewall-cmd --reload```

### 3. testons

- ça fonctionne pour ```10.105.0.2``` :
![](https://i.imgur.com/ZjyOFQd.png)

- ça fonctionne pour ```10.105.0.3``` :
![](https://i.imgur.com/Aq41kis.png)

- ça fonctionne pour ```10.105.0.4``` :
![](https://i.imgur.com/YzS1DaI.jpg)

## Fin.
