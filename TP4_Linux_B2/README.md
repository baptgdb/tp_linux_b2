# **TP4 : Conteneurs**

| docker1.tp4.linux | 10.104.1.2/24 |
| -------- | -------- |


## Sommaire

[TOC]


## I. Docker

### 1. Install


- sudo dnf check-update
- sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
- sudo dnf install docker-ce docker-ce-cli containerd.io
- sudo systemctl start docker
- sudo systemctl status docker
```
     Active: active (running)
```
- sudo systemctl enable docker

- on s'ajoute comme utilisateur à docker:
	- sudo usermod -aG docker $(whoami)

### 2. Vérifier l'install

- Info sur l'install actuelle de Docker:
	- docker info
```
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.9.1-docker)
  scan: Docker Scan (Docker Inc., v0.21.0)

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 0
 Server Version: 20.10.21
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: systemd
 Cgroup Version: 2
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 770bd0108c32f3fb5c73ae1264f7e503fe7b2661
 runc version: v1.1.4-0-g5fd4c4d
 init version: de40ad0
 Security Options:
  seccomp
   Profile: default
  cgroupns
 Kernel Version: 5.14.0-70.30.1.el9_0.x86_64
 Operating System: Rocky Linux 9.0 (Blue Onyx)
 OSType: linux
 Architecture: x86_64
 CPUs: 1
 Total Memory: 1.733GiB
 Name: docker1.tp4.linux
 ID: NHF3:7ZMD:WNTT:K6FW:UUNZ:DKVL:3PO3:IYSB:TADX:GZ2Q:SDLI:Q72T
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
```


- Liste des conteneurs actifs
	- docker ps
```CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES```
- Liste de tous les conteneurs
	- docker ps -a
```CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES```

- Liste des images disponibles localement
	- docker images
```REPOSITORY   TAG       IMAGE ID   CREATED   SIZE```

- Lancer un conteneur debian
	- docker run debian
```
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
a8ca11554fce: Pull complete
Digest: sha256:3066ef83131c678999ce82e8473e8d017345a30f5573ad3e44f62e5c9c46442b
Status: Downloaded newer image for debian:latest
```

- -d sert à mettre un conteneur en tâche de fond (-d pour daemon)
	- docker run -d debian sleep 99999


- docker ps
```

CONTAINER ID   IMAGE     COMMAND         CREATED         STATUS         PORTS     NAMES
29c93dfbdf67   debian    "sleep 99999"   2 minutes ago   Up 2 minutes             great_wescoff
```


- docker ps -a
```
CONTAINER ID   IMAGE     COMMAND         CREATED              STATUS                        PORTS     NAMES
4727308d87a8   debian    "bash"          45 seconds ago       Exited (130) 32 seconds ago             elastic_proskuriakova
9580b94102ba   debian    "bash"          About a minute ago   Exited (127) 57 seconds ago             gifted_satoshi
29c93dfbdf67   debian    "sleep 99999"   2 minutes ago        Up 2 minutes                            great_wescoff
604f680ab687   debian    "bash"          10 minutes ago       Exited (0) 10 minutes ago               nervous_austin
```

- docker images
```

REPOSITORY   TAG       IMAGE ID       CREATED      SIZE
debian       latest    c31f65dd4cc9   9 days ago   124MB
```

- supprimer un conteneur  donné, même s'il est allumé
	- docker rm -f
```
29c93dfbdf67
29c93dfbdf67
```

### 3. Lancement de conteneurs

- echo "toto baptiste" > index.htmls
- nano enginx.conf
```
server {
  # on définit le port où NGINX écoute dans le conteneur
  listen 9999;
  
  # on définit le chemin vers la racine web
  # dans ce dossier doit se trouver un fichier index.html
  root /var/www/tp4; 
}

```

- docker run -p 8888:9999  -d --memory="512m" --cpus="1.0" --name web -v $(pwd)/index.html:/var/www/tp4/index.html -v $(pwd)/toto.conf:/etc/nginx/conf.d/toto.conf nginx                                                                                                    
```
a108df4e04219fb30b87d516e5c2a51b5453466efd47e724e1b3dd92867a9e77
```
## II. Images

cat apache2.conf
```
# on définit un port sur lequel écouter
Listen 80

# on charge certains modules Apache strictement nécessaires à son bon fonctionnement
LoadModule mpm_event_module "/usr/lib/apache2/modules/mod_mpm_event.so"
LoadModule dir_module "/usr/lib/apache2/modules/mod_dir.so"
LoadModule authz_core_module "/usr/lib/apache2/modules/mod_authz_core.so"

# on indique le nom du fichier HTML à charger par défaut
DirectoryIndex index.html
# on indique le chemin où se trouve notre site
DocumentRoot "/var/www/html/"

# quelques paramètres pour les logs
ErrorLog "logs/error.log"
LogLevel warn

ServerName 10.104.1.2
```

cat index.html
```
toto baptiste
```

cat Dockerfile
```
FROM debian

RUN apt update -y

RUN apt install -y apache2

ADD . /etc/apache2/logs/

COPY ./apache2.conf /etc/apache2/apache2.conf

COPY ./index.html /var/www/html/index.html

CMD [ "apache2", "-D", "FOREGROUND" ]
```

docker build . -t my_own_image
```
Successfully built 752757d8eff6
Successfully tagged my_own_image:latest
```

docker images
```
REPOSITORY     TAG       IMAGE ID       CREATED          SIZE
my_own_image   latest    752757d8eff6   12 seconds ago   253MB
...
```

docker run -p 88:9999 my_own_image -g "daemon off;"


docker container prune
```
WARNING! This will remove all stopped containers.
...
```


[Dockerfile]()


## III. docker-compose


### 1. Intro

sudo curl -SL https://github.com/docker/compose/releases/download/v2.13.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose


### 2. Make your own

- pour cette partie, on utilise un projet existant, dans mon cas un prototype de cv en ligne 

- sudo yum install docker-compose-plugin

- on écrit un fichier qui décrit les conteneurs voulus

- dans le dossier app:
	- /home/baptiste/tp_linux_b2/TP4_Linux_B2/app

- dans ce dossier, il y a le fichier test.html qui est un test de cv en ligne, je n'ai pas eu le temps de faire la page en python avec flask, ou de documenter les projets.


cat [test.html](https://gitlab.com/baptgdb/tp_linux_b2/-/blob/main/TP4_Linux_B2/app/test.html):
```
<h1 id="test-web-cv-baptiste-gadebille">test web CV Baptiste Gadebille</h1>
<ul>
<li>Je suis étudiant en 2ème année, à Ynov Informatique Option Robotique.</li>
</ul>
<h2 id="comp-tences-">Compétences:</h2>
<ul>
<li>Bases en infrastructures système et réseau.</li>
<li>Langage de programmation en cours d&#39;apprentissage :<ul>
<li>Python, C++, C#</li>
</ul>
</li>
</ul>
<h2 id="formation-">formation:</h2>
<ul>
<li>Lycée Montesquieu Bordeaux, 2021.<ul>
<li>Baccalauréat, Mathématiques et SES.</li>
</ul>
</li>
</ul>
<h2 id="exp-riences-">expériences:</h2>
<ul>
<li><p><em>Monoprix CDD - Aout 2022</em></p>
<ul>
<li>Emploi commercial caisse</li>
</ul>
</li>
<li><p><em>Manutentionnaire Logistique - PSI Groupe Magellan  CDD Aout 2021</em></p>
<ul>
<li>Déploiement de flottes d&#39;ordinateurs</li>
</ul>
</li>
<li><p><em>Coopérant CJS - CDD (CJS Hello&#39;Coop) - Juillet - aout 2019</em></p>
<ul>
<li>Administration de la structure, secrétaire du conseil d&#39;administration </li>
</ul>
</li>
<li><em>Membre de l&#39;acelm  (Association lycéens) 2018 - 2021</em><ul>
<li>membre du Conseil d&#39;administration 2020-2021 </li>
</ul>
</li>
</ul>
<h2 id="projets-de-ydays-">projets de ydays:</h2>
<ul>
<li><p><em>Participation au labo Gearobot 2022-2023:</em></p>
<ul>
<li>Le projet est de concevoir un robot modulaire et polyvalent.</li>
</ul>
</li>
<li><p><em>Projet Robolympiades 2021-2022 &amp; 2022-2023:</em></p>
<ul>
<li>Compétition de robotique (conception mécatronique).<ul>
<li>Les robots sont autonome et s&#39;affrontent sur plusieurs épreuves.</li>
</ul>
</li>
</ul>
</li>
<li><p><em>Projet la bête humaine 2021-2022:</em></p>
<ul>
<li>Le projet est de faire un prototype de robot d&#39;accueil pour le salon de tatouage &quot;la bête humaine&quot;.</li>
</ul>
</li>
</ul>
<h2 id="projet-personnels-">Projet personnels:</h2>
<ul>
<li><p><em>Projet bb8:</em> </p>
<ul>
<li>Le but du projet est de faire un robot qui ressemble à celui de star wars, il se déplace à l&#39;intérieur d&#39;une sphère.</li>
</ul>
</li>
<li><p><em>Spider bot:</em></p>
<ul>
<li>Le but du projet est de concevoir un robot quadrupède et autonome .</li>
</ul>
</li>
</ul>
<h2 id="centres-d-int-r-t-">centres d&#39;intérêt :</h2>
<ul>
<li>La Robotique.</li>
<li>La natation.<ul>
<li>Club de natation pendant 11 ans.</li>
</ul>
</li>
<li>jeux vidéos:<ul>
<li>Jeux de gestion et technique.</li>
</ul>
</li>
<li>Lecture: <ul>
<li>Comics, Romans... </li>
</ul>
</li>
<li>Les Échecs.</li>
<li>L&#39;histoire de l’aérospatiale.</li>
</ul>
```

- il y a aussi le fichier [apache.conf](https://gitlab.com/baptgdb/tp_linux_b2/-/blob/main/TP4_Linux_B2/app/apache.conf):
```
# This is the main Apache server configuration file.  It contains the
# configuration directives that give the server its instructions.
# See http://httpd.apache.org/docs/2.4/ for detailed information about
# the directives and /usr/share/doc/apache2/README.Debian about Debian specific
# hints.
#
#
# Summary of how the Apache 2 configuration works in Debian:
# The Apache 2 web server configuration in Debian is quite different to
# upstream's suggested way to configure the web server. This is because Debian's
# default Apache2 installation attempts to make adding and removing modules,
# virtual hosts, and extra configuration directives as flexible as possible, in
# order to make automating the changes and administering the server as easy as
# possible.

# It is split into several files forming the configuration hierarchy outlined
# below, all located in the /etc/apache2/ directory:
#
#       /etc/apache2/
#       |-- apache2.conf
#       |       `--  ports.conf
#       |-- mods-enabled
#       |       |-- *.load
#       |       `-- *.conf
#       |-- conf-enabled
#       |       `-- *.conf
#       `-- sites-enabled
#               `-- *.conf
#
#
# * apache2.conf is the main configuration file (this file). It puts the pieces
#   together by including all remaining configuration files when starting up the
#   web server.
#
# * ports.conf is always included from the main configuration file. It is
#   supposed to determine listening ports for incoming connections which can be
#   customized anytime.
#
# * Configuration files in the mods-enabled/, conf-enabled/ and sites-enabled/
#   directories contain particular configuration snippets which manage modules,
#   global configuration fragments, or virtual host configurations,
#   respectively.
#
#   They are activated by symlinking available configuration files from their
#   respective *-available/ counterparts. These should be managed by using our
#   helpers a2enmod/a2dismod, a2ensite/a2dissite and a2enconf/a2disconf. See
#   their respective man pages for detailed information.
#
# * The binary is called apache2. Due to the use of environment variables, in
#   the default configuration, apache2 needs to be started/stopped with
#   /etc/init.d/apache2 or apache2ctl. Calling /usr/bin/apache2 directly will not
#   work with the default configuration.


# Global configuration
#

#
# ServerRoot: The top of the directory tree under which the server's
# configuration, error, and log files are kept.
#
# NOTE!  If you intend to place this on an NFS (or otherwise network)
# mounted filesystem then please read the Mutex documentation (available
# at <URL:http://httpd.apache.org/docs/2.4/mod/core.html#mutex>);
# you will save yourself a lot of trouble.
#
# Do NOT add a slash at the end of the directory path.
#
#ServerRoot "/etc/apache2"

#
# The accept serialization lock file MUST BE STORED ON A LOCAL DISK.
#
#Mutex file:${APACHE_LOCK_DIR} default

#
# The directory where shm and other runtime files will be stored.
#

DefaultRuntimeDir /var/run/apache2

#
# PidFile: The file in which the server should record its process
# identification number when it starts.
# This needs to be set in /etc/apache2/envvars
#
PidFile ${APACHE_PID_FILE}

#
# Timeout: The number of seconds before receives and sends time out.
#
Timeout 300

#
# KeepAlive: Whether or not to allow persistent connections (more than
# one request per connection). Set to "Off" to deactivate.
#
KeepAlive On

#
# MaxKeepAliveRequests: The maximum number of requests to allow
# during a persistent connection. Set to 0 to allow an unlimited amount.
# We recommend you leave this number high, for maximum performance.
#
MaxKeepAliveRequests 100

#
# KeepAliveTimeout: Number of seconds to wait for the next request from the
# same client on the same connection.
#
KeepAliveTimeout 5


# These need to be set in /etc/apache2/envvars
User www-data
Group www-data

#
# HostnameLookups: Log the names of clients or just their IP addresses
# e.g., www.apache.org (on) or 204.62.129.132 (off).
# The default is off because it'd be overall better for the net if people
# had to knowingly turn this feature on, since enabling it means that
# each client request will result in AT LEAST one lookup request to the
# nameserver.
#
HostnameLookups Off

# ErrorLog: The location of the error log file.
# If you do not specify an ErrorLog directive within a <VirtualHost>
# container, error messages relating to that virtual host will be
# logged here.  If you *do* define an error logfile for a <VirtualHost>
# container, that host's errors will be logged there and not here.
#
ErrorLog /var/log/apache2/error.log

#
# LogLevel: Control the severity of messages logged to the error_log.
# Available values: trace8, ..., trace1, debug, info, notice, warn,
# error, crit, alert, emerg.
# It is also possible to configure the log level for particular modules, e.g.
# "LogLevel info ssl:warn"
#
LogLevel warn

# Include module configuration:
IncludeOptional mods-enabled/*.load
IncludeOptional mods-enabled/*.conf

# Include list of ports to listen on
Include ports.conf


# Sets the default security model of the Apache2 HTTPD server. It does
# not allow access to the root filesystem outside of /usr/share and /var/www.
# The former is used by web applications packaged in Debian,
# the latter may be used for local directories served by the web server. If
# your system is serving content from a sub-directory in /srv you must allow
# access here, or in any related virtual host.
<Directory />
        Options FollowSymLinks
        AllowOverride None
        Require all denied
</Directory>

<Directory /usr/share>
        AllowOverride None
        Require all granted
</Directory>

<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
</Directory>

#<Directory /srv/>
#       Options Indexes FollowSymLinks
#       AllowOverride None
#       Require all granted
#</Directory>




# AccessFileName: The name of the file to look for in each directory
# for additional configuration directives.  See also the AllowOverride
# directive.
#
AccessFileName .htaccess

#
# The following lines prevent .htaccess and .htpasswd files from being
# viewed by Web clients.
#
<FilesMatch "^\.ht">
        Require all denied
</FilesMatch>


#
# The following directives define some format nicknames for use with
# a CustomLog directive.
#
# These deviate from the Common Log Format definitions in that they use %O
# (the actual bytes sent including headers) instead of %b (the size of the
# requested file), because the latter makes it impossible to detect partial
# requests.
#
# Note that the use of %{X-Forwarded-For}i instead of %h is not recommended.
# Use mod_remoteip instead.
#
LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined
LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %O" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent

# Include of directories ignores editors' and dpkg's backup files,
# see README.Debian for details.

# Include generic snippets of statements
IncludeOptional conf-enabled/*.conf

# Include the virtual host configurations:
IncludeOptional sites-enabled/*.conf

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

- ainsi que le fichier [Dockerfile](https://gitlab.com/baptgdb/tp_linux_b2/-/blob/main/TP4_Linux_B2/app/Dockerfile):
```
FROM debian

RUN apt update -y

RUN apt install -y apache2 && \
    rm /etc/apache2/sites-enabled/000-default.conf \
       /etc/apache2/conf-enabled/other-vhosts-access-log.conf

ADD . /etc/apache2/logs/

COPY ./test.conf /etc/apache2/conf-enabled/test.conf
COPY ./apache2.conf /etc/apache2/apache2.conf


RUN mkdir -p /var/run/apache2 /var/www/tp4 && \
    chown www-data:www-data -R /var/run/apache2 /var/log/apache2 /var/www/tp4

COPY ./test.html /var/www/tp4/index.html

USER www-data

ENV APACHE_PID_FILE=/var/run/apache2/apache2.pid

CMD [ "apache2", "-D", "FOREGROUND" ]

```


docker build . -t new_image
```
...
Successfully built e9c09e7ac13a
Successfully tagged new_image:latest
```


[docker-compose.yml](https://gitlab.com/baptgdb/tp_linux_b2/-/blob/main/TP4_Linux_B2/app/docker-compose.yml)
```
version: '3.9'
services:
  apache:
    image: httpd:latest
    container_name: new_image
    ports:
    - '8080:90'
```
